/*
* Hint Plugin
*
* @author Sagar Boina <sagar.sijju@gmail.com>
* @version 1.0
*/
(function () {
    CKEDITOR.plugins.add( 'hint', {
        icons: 'hint',
        init: function( editor ) {
            editor.addCommand( 'hint', new CKEDITOR.dialogCommand( 'hint' ) );
            editor.ui.addButton( 'Hint', {
                label: 'Add Hint',
                command: 'hint',
                toolbar: 'insert'
     
            });
            let plugin_path = this.path;
			
			
			CKEDITOR.dialog.add('hint', function (editor) {
				return {
					title: 'Hint',
					minWidth: 400,
					minHeight: 200,
					contents: [
						{
							id: 'HintPlugin',
							label: 'Basic Settings',
							elements: [
								{
									type: 'textarea',
									id: 'hint_text',
									label: 'Hint',
									setup: function( element ) {
										this.setValue( element.getText() );
									},
									commit: function( element ) {
										element.setText( this.getValue() );
									}
								}
							]
						}
					],
					onShow: function() {
						var selection = editor.getSelection();
						var element = selection.getStartElement();
						this.element = element;
						this.setupContent( this.element );						
					},
					onOk: function() {
						var txt = this.getValueOf('HintPlugin', 'hint_text');					
						var img_src = plugin_path + 'icons/hint.png';
						editor.insertHtml( "<span class='ck-hint-container'><img src='"+img_src+"'> "+txt+"</span>" );
					}
				}
			}); //dialog ends
    } //init ends
	}) //add ends
})()